# Cilj učne aktivnosti #

Cilj je motivirati udeležence, da je v osnovi upravljanje s programsko in strojno opremo kot bi sestavljali lego kocke. Začeti je potrebno pri malih in enostavnih kockah/konceptih, vendar je z veliko volje mogoče zgraditi dih jemajoče reči. Aktivnost naj služi udeležencem tudi kot odskočna deska za nadaljevanje eksperimentiranja v domačem okolju. Zato aktivnost predstavi osnovne programske koncepte (vejitve, spremenljivke, zanke, itd.), Raspberry PI (v nadaljevanju kar RPI) ter ostalih strojnih komponent (senzorji, tipke, LED diode, itd.). Programiranje zna biti za marsikoga dolgočasno, če nima rezultat dela fizičnega učinka v realnem svetu. Udeleženci na delavnici z uporabo različnih osnovnih tehnik programiranja in z glasbeno podlago ustvarijo različne algoritme za utripanja LED diod.


# Ciljna skupina #

Glavna fokusna skupina so predvsem srednješolci gimnazijske ali strokovne usmeritve z minimalnim znanjem programiranja oz. vsaj poznavanjem delovanja “diagram poteka”. Skupina udeležencev naj bo manjša in zeželjeno je delo v parih. Glede na predhodnje znanje dotične skupine predavatelj izbere ali bo uporabil pravi programski jezik (npr. Python) ali bo uporabljal vizualne nadomestke ukazov preko grafičnega vmesnika. Platforma Wyliodrin to omogoča brez posebnega napora za predavatelja.


# Gradiva, orodja, naprave in ostale potrebne komponente #

Priporočen en komplet spodaj naštetih komponent na dva (do tri) udeležence.
- registracija projekta na platformi za vizualno programiranje Wyliodrin -
https://www.wyliodrin.com/ (lahko se platformo naloži tudi lokalno);
- računalnik;
- Raspberry PI (2. ali 3. generacije);
- 5x LED diod;
- protoboard (“breadboard”);
- 5x 220Ohm uporov;
- “jumper” žice;


# Trajanje aktivnosti #

Veliko je odvisno od angažiranosti in predznanja udeležencev, vendar se priporoča vsaj 4 polne ure, za čiste začetnike vsaj 6 ur.


# Trenutno so vsa gradiva in navodila gostovana na spodnji povezavi #

https://drive.google.com/drive/folders/0B1AyGS3PkAxfTkRBeEVGbHN5d28



![prototip photo] (https://drive.google.com/uc?id=0B1yfSN5YNf0_WmtfQ01kOEZlZzQ&export=download)